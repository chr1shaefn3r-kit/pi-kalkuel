## Encoding: ISO8859-1 ##

VORTRAG_1 = pi-kalkuel_1
VORTRAG_2 = pi-kalkuel_2
ZIPNAME = pi-kalkuel-LaTeX-Beamer.zip
.PHONY: clean

v1: *.tex *.bib
	pdflatex $(VORTRAG_1).tex
	bibtex $(VORTRAG_1)
	pdflatex $(VORTRAG_1).tex
	pdflatex $(VORTRAG_1).tex
	
v1f: *.tex *.bib
	pdflatex $(VORTRAG_1).tex	
	
v2: *.tex *.bib
	pdflatex $(VORTRAG_2).tex
	bibtex $(VORTRAG_2)
	pdflatex $(VORTRAG_2).tex
	pdflatex $(VORTRAG_2).tex
	
v2f: *.tex *.bib
	pdflatex $(VORTRAG_2).tex	

clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.nav
	rm -f *.out
	rm -f *.snm
	rm -f *.toc
	rm -f *.bbl
	rm -f *.blg
	rm -f *.zip
	rm -f *~

zip: clean
	zip $(ZIPNAME) *
	zip $(ZIPNAME) logos/*
	zip $(ZIPNAME) bilder/*

bundle: v1 v2 clean zip

